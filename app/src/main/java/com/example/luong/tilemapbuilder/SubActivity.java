package com.example.luong.tilemapbuilder;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

/**
 * Created by Luong on 2016-07-11.
 */
public class SubActivity
    extends AppCompatActivity
{
    //////////////////////////////////////////////////
    // Constant Variable

    public final static String TAG = SubActivity.class.getSimpleName();

    //////////////////////////////////////////////////
    // region // Creation Methods

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        Log.d( TAG, "Creating Sub-activity..." );

        // Change app icon into a clickable icon
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        Log.d( TAG, "Sub-activity created..." );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Menu bar Methods

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch( item.getItemId() )
        {
            case android.R.id.home:
                Log.d( TAG, "Home action was selected." );
                gotoMainActivity();
                break;
            default:
                Log.d( TAG, "Action not found." );
                return super.onOptionsItemSelected( item );
        }

        return true;
    }

    // This method will bring the user back to the main activity.
    //
    public void gotoMainActivity()
    {
        Log.d( TAG, "Going to main activity..." );

        Intent intentHome = new Intent( this , MainActivity.class );
        intentHome.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );

        startActivity( intentHome );
    }

    // endregion
}
