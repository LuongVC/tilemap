package com.example.luong.tilemapbuilder.Classes;

/**
 * Created by Luong on 2016-09-18.
 */
public class Vector
{
    // Add Vector1 and Vector2
    //
    public static Point2d addPt1AndPt2( Point2d pt1 , Point2d pt2 )
    {
        return new Point2d( pt1.x+pt2.x , pt1.y+pt2.y );
    }

    // Add Vector1 and Vector2
    //
    public static Point3d addPt2ToPt1( Point3d pt1 , Point3d pt2 )
    {
        return new Point3d( pt1.x+pt2.x , pt1.y+pt2.y , pt1.z + pt2.z );
    }

    // Subtract Vector2 from Vector1
    //
    public static Point2d subtractPt2FromPt1( Point2d pt1 , Point2d pt2 )
    {
        return new Point2d( pt1.x-pt2.x , pt1.y-pt2.y );
    }

    // Subtract Vector2 from Vector1
    //
    public static Point3d subtractPt2FromPt1( Point3d pt1 , Point3d pt2 )
    {
        return new Point3d( pt1.x-pt2.x , pt1.y-pt2.y , pt1.z-pt2.z );
    }

    // Scale Vector1 by K (Real Number)
    //
    public static Point2d scale( float k , Point2d pt1 )
    {
        return new Point2d( k*pt1.x , k*pt1.y );
    }

    // Scale Vector1 by K (Real Number)
    //
    public static Point3d scale( float k , Point3d pt1 )
    {
        return new Point3d( k*pt1.x , k*pt1.y , k*pt1.z );
    }

    // Normalize Vector
    //
    public static Point2d normalize( Point2d pt1 )
    {
        float magnitude = magnitudeOf( pt1 );
        return new Point2d( pt1.x / magnitude , pt1.y / magnitude );
    }

    // Normalize Vector
    //
    public static Point3d normalize( Point3d pt1 )
    {
        float magnitude = magnitudeOf( pt1 );
        return new Point3d( pt1.x / magnitude , pt1.y / magnitude , pt1.z );
    }

    // Find the magnitude of Vector1
    //
    public static float magnitudeOf( Point2d pt1 )
    {
        return (float)Math.sqrt( pt1.x*pt1.x + pt1.y*pt1.y );
    }

    // Find the magnitude of Vector1
    //
    public static float magnitudeOf( Point3d pt1 )
    {
        return (float)Math.sqrt( pt1.x*pt1.x + pt1.y*pt1.y + pt1.z*pt1.z );
    }

    // Find the dot product of Vector1 and Vector2
    //
    public static float dotProductOf( Point2d pt1 , Point2d pt2 )
    {
        return ( pt1.x*pt2.x + pt1.y*pt2.y );
    }

    // Find the dot product of Vector1 and Vector2
    //
    public static float dotProductOf( Point3d pt1 , Point3d pt2 )
    {
        return ( pt1.x*pt2.x + pt1.y*pt2.y + pt1.z*pt2.z );
    }

    // Find the angle between Vector1 and Vector2
    //
    public static float angleOf( Point2d pt1 , Point2d pt2 )
    {
        return (float)Math.acos( dotProductOf( pt1 , pt2 ) /
                ( magnitudeOf( pt1 ) * magnitudeOf( pt2 ) ) );
    }

    // Find the angle between Vector1 and Vector2
    //
    public static float angleOf( Point3d pt1 , Point3d pt2 )
    {
        return (float)Math.acos( dotProductOf( pt1 , pt2 ) /
                ( magnitudeOf( pt1 ) * magnitudeOf( pt2 ) ) );
    }

    public static Point3d rotate( Point2d pt1 , Point3d normal , float angle )
    {
        return new Point3d();
    }

    // Find the cross product of Vector1 and Vector2
    //
    public static Point3d crossProduct( Point3d pt1 , Point3d pt2 )
    {
        return new Point3d( pt1.y*pt2.z - pt1.z*pt2.y ,
                            pt1.z*pt2.x - pt1.x*pt2.z ,
                            pt1.x*pt2.y - pt1.y*pt2.x );
    }
}
