package com.example.luong.tilemapbuilder.Widgets;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Luong on 2016-08-05.
 */
public class IsometricMapView
    extends MapView
{
    //////////////////////////////////////////////////
    // Constant Variable

    public final static String TAG = IsometricMapView.class.getSimpleName();



    //////////////////////////////////////////////////
    // Data Members

    protected int mGridWidth;
    protected int mGridHeight;



    //////////////////////////////////////////////////
    // Constructor

    public IsometricMapView( Context context , AttributeSet attrs )
    {
        super( context , attrs );
    }
}
