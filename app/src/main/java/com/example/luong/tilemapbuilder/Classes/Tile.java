package com.example.luong.tilemapbuilder.Classes;

/**
 * Base tile class.
 *
 * Created by LuongVC.
 */

public class Tile
{
    //////////////////////////////////////////////////
    // Data Members

    public int tileTypeId;
    public int tileId;
    public int x;
    public int y;

    //////////////////////////////////////////////////
    // Constructors

    public Tile()
    {
        tileTypeId  = TileFactory.BASTE_TILE;
        tileId      = 0;
        x           = 0;
        y           = 0;
    }

    public Tile( int x , int y )
    {
        tileTypeId  = TileFactory.BASTE_TILE;
        tileId      = 0;
        this.x      = x;
        this.y      = y;
    }

    //////////////////////////////////////////////////
    // Copy Constructors

    public Tile( Tile tile )
    {
        tileTypeId  = tile.tileTypeId;
        tileId      = tile.tileId;
        x           = tile.x;
        y           = tile.y;
    }
}
