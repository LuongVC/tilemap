package com.example.luong.tilemapbuilder;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

import com.example.luong.tilemapbuilder.Classes.Map;

/**
 * Map builder activity used to display the tools to build a map.
 *
 * Created by Luong.
 */
public class MapBuilderActivity
        extends SubActivity
        implements MapPropertiesFragment.IOnMapPropertiesUpdateListener
{
    //////////////////////////////////////////////////
    // region // Constant Variable

    public final static String Tag              = MapBuilderActivity.class.getSimpleName();

    // endregion

    //////////////////////////////////////////////////
    // region // Member Variables

    MapBuilderFragment mMapBuilder;
    MapPropertiesFragment mMapProperties;

    // endregion

    //////////////////////////////////////////////////
    // region // Constructor

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        Log.d( TAG, "Creating Map Builder activity..." );

        setContentView( R.layout.activity_map_builder );

        // Setup map properties fragment
        Log.d( TAG, "Setting up Map Properties..." );

        Bundle bundle = new Bundle();
        bundle.putParcelable( "Map" , getIntent().getExtras().getParcelable( "Map" ) );

        mMapBuilder = new MapBuilderFragment();
        mMapBuilder.setArguments( bundle );

        mMapProperties = new MapPropertiesFragment();
        mMapProperties.setArguments( bundle );

        // Replacing layout content with fragment
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace( R.id.fl_content_view , mMapBuilder ,
                mMapBuilder.getClass().getSimpleName() );
        transaction.replace( R.id.nav_drawer_view , mMapProperties ,
                mMapProperties.getClass().getSimpleName() );
        transaction.commit();

        Log.d( TAG, "Map Builder activity created." );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // IOnMapPropertiesUpdateListener Methods

    @Override
    public void onMapPropertiesUpdate( Map map )
    {
        mMapBuilder.loadMap( map );
    }

    // endregion
}
