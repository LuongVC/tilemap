package com.example.luong.tilemapbuilder.Widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;

import com.example.luong.tilemapbuilder.Classes.SquareTile;

/**
 * Created by Luong on 2016-08-05.
 */
public class OrthogonalMapView
        extends MapView
{
    //////////////////////////////////////////////////
    // Constant Variable

    public final static String TAG = OrthogonalMapView.class.getSimpleName();



    //////////////////////////////////////////////////
    // Data Members

    protected SquareTile squareTile;



    //////////////////////////////////////////////////
    // Constructor

    public OrthogonalMapView( Context context , AttributeSet attrs )
    {
        super( context , attrs );

        Log.d( TAG, "OrthogonalMapView: Started" );

        squareTile = new SquareTile( getMapTileWidth() , getMapTileHeight() );

        Log.d( TAG, "OrthogonalMapView: Ended" );
    }



    //////////////////////////////////////////////////
    // Draw Methods

    @Override
    public void onDraw( Canvas canvas )
    {
        drawGrid( canvas );
    }

    protected void drawGrid( Canvas canvas )
    {
        for( int y = 0 ; y < getMapHeight() ; y++ )
        {
            for( int x = 0 ; x < getMapWidth() ; x++ )
            {
                squareTile.drawTile( x * getMapTileWidth() , y * getMapTileHeight() , canvas );
            }
        }
    }
}
