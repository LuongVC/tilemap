package com.example.luong.tilemapbuilder;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luong.tilemapbuilder.Classes.Map;
import com.example.luong.tilemapbuilder.Classes.MapParcelable;
import com.example.luong.tilemapbuilder.Classes.Utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Map property fragment used to manage the map settings within the navigation drawer.
 *
 * Created by Luong.
 */
public class MapPropertiesFragment
        extends Fragment
        implements View.OnClickListener ,
                   AdapterView.OnItemSelectedListener
{
    //////////////////////////////////////////////////
    // region // Constant Variables

    public final static String  TAG             = CreateMapDialogFragment.class.getSimpleName();

    public final static int     INVALID_SIZE    = -1;

    // endregion

    //////////////////////////////////////////////////
    // region // Widget Controls

    protected LinearLayout mLlHeader;

    protected EditText mTbMapName;

    protected Spinner mSpnMapType;
    protected EditText mTbMapWidth;
    protected EditText mTbMapHeight;

    protected Spinner mSpnMapTileType;
    protected CheckBox mCbMapTileRatio;
    protected TextView mTvMapTileWidth;
    protected TextView mTvMapTileHeight;
    protected EditText mTbMapTileWidth;
    protected EditText mTbMapTileHeight;

    protected TextView mTvShowMoreSettings;
    protected Button mBtnUpdateMap;

    // endregion

    //////////////////////////////////////////////////
    // region // Data Members

    protected Map mMap;

    protected boolean mIsMoreSettingVisible;
    protected float mTileWidthToHeightRatio;

    // endregion

    //////////////////////////////////////////////////
    // region // Callback Members

    IOnMapPropertiesUpdateListener mCallback;

    // endregion

    //////////////////////////////////////////////////
    // region // Constructors

    public View onCreateView(
            LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState )
    {
        Log.d( TAG, "Creating Map Properties fragment..." );

        // inflate the map property layout
        View view = inflater.inflate( R.layout.fragment_map_properties , null , false );

        // Setup the widget controls
        initWidgets( view );
        populateMapTypeSpinner();
        populateMapTileTypeSpinner();
        regWidgetListeners();

        showHeader( true );
        showMoreSettings( false );

        // Load data
        Bundle bundle = getArguments();
        if( bundle != null )
        {
            MapParcelable mapParcelable = bundle.getParcelable( "Map" );
            loadMap( mapParcelable.getMap() );
        }
        else
        {
            loadMap( createMap() );
        }

        Log.d( TAG, "Map Properties fragment created." );

        return view;
    }

    // Method used to initialize variables for widget controls.
    //
    protected void initWidgets( View view )
    {
        Log.d( TAG, "Initializing widget controls..." );

        // Get layouts
        mLlHeader           = (LinearLayout)view.findViewById( R.id.ll_header );

        // Get widget controls
        mTbMapName          = (EditText)view.findViewById( R.id.tb_map_name );

        mSpnMapType         = (Spinner)view.findViewById( R.id.spn_map_type );
        mTbMapWidth         = (EditText)view.findViewById( R.id.tb_map_width );
        mTbMapHeight        = (EditText)view.findViewById( R.id.tb_map_height );

        mSpnMapTileType     = (Spinner)view.findViewById( R.id.spn_map_tile_type );
        mCbMapTileRatio     = (CheckBox)view.findViewById( R.id.cb_calculate_map_tile_ratio );
        mTvMapTileWidth     = (TextView)view.findViewById( R.id.tv_map_tile_width );
        mTvMapTileHeight    = (TextView)view.findViewById( R.id.tv_map_tile_height );
        mTbMapTileWidth     = (EditText)view.findViewById( R.id.tb_map_tile_width );
        mTbMapTileHeight    = (EditText)view.findViewById( R.id.tb_map_tile_height );

        mTvShowMoreSettings = (TextView)view.findViewById( R.id.tv_show_more_settings );
        mBtnUpdateMap       = (Button)view.findViewById( R.id.btn_update_map );

        Log.d( TAG, "Widget Controls initialized." );
    }

    // Method used to assigned, register, and setup listeners for widget controls.
    //
    protected void regWidgetListeners()
    {
        Log.d( TAG, "Registering widget controls listener..." );

        // Set widget listener
        mSpnMapType.setOnItemSelectedListener( this );
        mSpnMapTileType.setOnItemSelectedListener( this );
        mTvShowMoreSettings.setOnClickListener( this );
        mCbMapTileRatio.setOnClickListener( this );
        mBtnUpdateMap.setOnClickListener( this );

        mTbMapTileWidth.addTextChangedListener( new TextWatcher(){
            @Override
            public void beforeTextChanged( CharSequence charSequence, int i, int i1, int i2 )
            {

            }

            @Override
            public void onTextChanged( CharSequence charSequence, int i, int i1, int i2 )
            {
                if( mCbMapTileRatio.isChecked() && mTbMapTileWidth.isFocused() )
                {
                    updateMapTileHeight();
                }
            }

            @Override
            public void afterTextChanged( Editable editable )
            {

            }
        } );

        mTbMapTileHeight.addTextChangedListener( new TextWatcher(){
            @Override
            public void beforeTextChanged( CharSequence charSequence, int i, int i1, int i2 )
            {

            }

            @Override
            public void onTextChanged( CharSequence charSequence, int i, int i1, int i2 )
            {
                if( mCbMapTileRatio.isChecked() && mTbMapTileHeight.isFocused() )
                {
                    updateMapTileWidth();
                }
            }

            @Override
            public void afterTextChanged( Editable editable )
            {

            }
        } );

        Log.d( TAG, "Widget controls listener registered." );
    }

    // Creates an adapter to populate the MapType dropdown list.
    //
    protected void populateMapTypeSpinner()
    {
        Log.d( TAG, "Populating Map Type dropdown list..." );

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource( getActivity() ,
                R.array.map_type_array, android.R.layout.simple_spinner_item );
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        mSpnMapType.setAdapter( adapter );

        Log.d( TAG, "Map Type dropdown list populated." );
    }

    // Creates an adapter to populate the MapTileType dropdown list.
    //
    protected void populateMapTileTypeSpinner()
    {
        Log.d( TAG, "Populating Map Tile Type dropdown list..." );

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource( getActivity() ,
                R.array.map_tile_type_array, android.R.layout.simple_spinner_item );
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        mSpnMapTileType.setAdapter( adapter );

        Log.d( TAG, "Map Tile Type dropdown list populated." );
    }

    // Method use to show or hide the header display of the fragment layout.
    //
    protected void showHeader( boolean isVisible )
    {
        if( isVisible == true )
        {
            mLlHeader.setVisibility( View.VISIBLE );
            Log.d( TAG, "Header is visible." );
        }
        else
        {
            mLlHeader.setVisibility( View.GONE );
            Log.d( TAG, "Header is gone." );
        }
    }

    // Method use to show or hide additional settings for the map.
    //
    protected void showMoreSettings( boolean isVisible )
    {
        mIsMoreSettingVisible = isVisible;

        if( isVisible == true )
        {
            // Change text value and colour
            mTvShowMoreSettings.setText( getResources()
                    .getString( R.string.txt_hide_more_properties ) );
            mTvShowMoreSettings.setTextColor( getResources()
                    .getColor( R.color.color_hide_more_properties ));

            // Shows additional settings
            mCbMapTileRatio.setVisibility( View.VISIBLE );

            mTvMapTileWidth.setVisibility( View.VISIBLE );
            mTvMapTileHeight.setVisibility( View.VISIBLE );
            mTbMapTileWidth.setVisibility( View.VISIBLE );
            mTbMapTileHeight.setVisibility( View.VISIBLE );

            Log.d( TAG, "More Settings is visible." );
        }
        else
        {
            // Change text value and colour
            mTvShowMoreSettings.setText( getResources()
                    .getString( R.string.txt_show_more_properties ) );
            mTvShowMoreSettings.setTextColor( getResources()
                    .getColor( R.color.color_show_more_properties ));

            // Hides additional settings
            mTbMapTileWidth.setVisibility( View.GONE );
            mTbMapTileHeight.setVisibility( View.GONE );

            mCbMapTileRatio.setVisibility( View.GONE );

            mTvMapTileWidth.setVisibility( View.GONE );
            mTvMapTileHeight.setVisibility( View.GONE );

            Log.d( TAG, "More Settings is gone." );
        }
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Setters

    // Method used to create a new map.
    //
    public Map createMap()
    {
        // Get default values for preferences
        String defaultMapType    = getResources().getString( R.string.pref_summary_map_type );

        int defaultMapWidth      = getResources().getInteger( R.integer.pref_value_map_size_width );
        int defaultMapHeight     = getResources().getInteger( R.integer.pref_value_map_size_height );

        int defaultMapTileWidth  = getResources().getInteger( R.integer.pref_value_tile_size_width );
        int defaultMapTileHeight = getResources().getInteger( R.integer.pref_value_tile_size_height );

        // Get default values from user preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( getActivity() );

        defaultMapType          = prefs.getString( "prefMapType" , defaultMapType );
        ArrayAdapter aaMapType  = (ArrayAdapter)mSpnMapType.getAdapter();
        int mapType = aaMapType.getPosition( defaultMapType );

        int mapWidth = Integer.parseInt( prefs.getString( "prefMapWidth" ,
                Integer.toString( defaultMapWidth ) ) );
        int mapHeight = Integer.parseInt( prefs.getString( "prefMapHeight" ,
                Integer.toString( defaultMapHeight ) ) );

        defaultMapType              = prefs.getString( "prefMapTileType" , defaultMapType );
        ArrayAdapter aaMapTileType  = (ArrayAdapter)mSpnMapTileType.getAdapter();
        int mapTileType = aaMapTileType.getPosition( defaultMapType );

        int mapTileWidth = Integer.parseInt( prefs.getString( "prefMapTileWidth" ,
                Integer.toString( defaultMapTileWidth ) ) );
        int mapTileHeight = Integer.parseInt( prefs.getString( "prefMapTileHeight" ,
                Integer.toString( defaultMapTileHeight ) ) );

        // Create the map
        Map mapCreated = new Map();
        mapCreated.setMapType( mapType );
        mapCreated.setMapWidth( mapWidth );
        mapCreated.setMapHeight( mapHeight );
        mapCreated.setMapTileType( mapTileType );
        mapCreated.setMapTileWidth( mapTileWidth );
        mapCreated.setMapTileHeight( mapTileHeight );

        return mapCreated;
    }

    // Method used to populate the map properties corresponding to the map data.
    //
    public boolean loadMap( Map map )
    {
        boolean isLoaded = false;

        if( map != null )
        {
            mMap = map;

            // Display the map properties within the correspond views
            mTbMapName.setText( map.getMapName() );

            mSpnMapType.setSelection( map.getMapTypeId() );
            mTbMapWidth.setText( Integer.toString( map.getMapWidth() ) );
            mTbMapHeight.setText( Integer.toString( map.getMapHeight() ) );

            mSpnMapTileType.setSelection( map.getMapTileTypeId() );
            mTbMapTileWidth.setText( Integer.toString( map.getMapTileWidth() ) );
            mTbMapTileHeight.setText( Integer.toString( map.getMapTileHeight() ) );

            isLoaded = true;
            Log.d( TAG, "Loading map was successful." );
        }
        else
        {
            Log.d( TAG, "Loading map was unsuccessful." );
        }

        return isLoaded;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Getters

    // Returns a validated bundle containing all the map properties enter by the user.
    //
    public Map getMap()
    {
        Map mapValidated = null;
        if( validateMapProperties() == true )
        {
            mapValidated = mMap;
        }

        return mapValidated;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Override Listener Methods

    @Override
    public void onClick( View view )
    {
        switch( view.getId() )
        {
            case R.id.cb_calculate_map_tile_ratio:
                if( mCbMapTileRatio.isChecked() == true ){ calculateTileRatio(); }
                break;
            case R.id.tv_show_more_settings:
                showMoreSettings( !mIsMoreSettingVisible );
                break;
            case R.id.btn_update_map:
                updateMapSettings();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemSelected( AdapterView< ? > adapterView, View view, int i, long l )
    {
        switch( adapterView.getId() )
        {
            case R.id.spn_map_type:
                mMap.setMapType( adapterView.getSelectedItemPosition() );
                Log.d( TAG, "Changing Map Type: " + mMap.getMapTypeId() );
                break;
            case R.id.spn_map_tile_type:
                mMap.setMapTileType( adapterView.getSelectedItemPosition() );
                Log.d( TAG, "Changing Map Tile Type: " + mMap.getMapTileTypeId() );
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected( AdapterView< ? > adapterView )
    {

    }

    // endregion

    //////////////////////////////////////////////////
    // region // Map Tile Property Methods

    // Method will calculate and store the current tile width-to-height ratio, if the user
    // choose to keep the tile proportions, and will call the corresponding methods to adjust the
    // tile width or height size.
    //
    protected void calculateTileRatio()
    {
        Log.d( TAG, "Calculating Tile's width-to-height ratio..." );

        try
        {
            // Get the tile dimension
            int mapTileWidth = Integer.parseInt( mTbMapTileWidth.getText().toString() );
            int mapTileHeight = Integer.parseInt( mTbMapTileHeight.getText().toString() );

            // Calculate map tile width to height ratio
            mTileWidthToHeightRatio = (float)mapTileHeight / mapTileWidth;
        }
        catch( NumberFormatException e )
        {
            mTileWidthToHeightRatio = 1.0f;
        }

        Log.d( TAG, "Tile's width-to-height ratio calculated: " + mTileWidthToHeightRatio );
    }

    // Method calculate and update the tile width base on the current tile height.
    //
    protected void updateMapTileWidth()
    {
        Log.d( TAG, "Updating Tile width..." );

        try
        {
            // Get the map tile height
            int mapTileHeight = Integer.parseInt( mTbMapTileHeight.getText().toString() );

            // Calculate the map tile width
            int mapTileWidth = (int)( mapTileHeight / mTileWidthToHeightRatio );

            // Update the map tile dimension
            mMap.setMapTileWidth( mapTileWidth );
            mMap.setMapTileHeight( mapTileHeight );

            // Update the map tile width view
            mTbMapTileWidth.setText( Integer.toString( mapTileWidth ) );
        }
        catch( NumberFormatException e )
        {
            // If height is empty, make width empty as well
            if( mTbMapTileHeight.getText().toString().isEmpty() )
            {
                mTbMapTileWidth.setText( "" );
            }

            // Else do nothing
        }

        Log.d( TAG, "Tile width updated." );
    }

    // Method calculate and update the tile height base on the current tile width.
    //
    protected void updateMapTileHeight()
    {
        Log.d( TAG, "Updating Tile height..." );

        try
        {
            // Get the map tile width
            int mapTileWidth = Integer.parseInt( mTbMapTileWidth.getText().toString() );

            // Calculate the tile height
            int mapTileHeight = (int)( mapTileWidth * mTileWidthToHeightRatio );

            // Update the map tile dimension
            mMap.setMapTileWidth( mapTileWidth );
            mMap.setMapTileHeight( mapTileHeight );

            // Update the map tile height view
            mTbMapTileHeight.setText( Integer.toString( mapTileHeight ) );
        }
        catch( NumberFormatException e )
        {
            // If width is empty, make height empty as well
            if( mTbMapTileWidth.getText().toString().isEmpty() )
            {
                mTbMapTileHeight.setText( "" );
            }

            // Else do nothing
        }

        Log.d( TAG, "Tile height updated." );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Update Methods

    // Returns whether or not all the user input are valid.
    //
    protected boolean validateMapProperties()
    {
        Log.d( TAG, "Validating Map Properties..." );

        boolean bIsValid = true;
        String strErrMessage = "";

        // Get resource values
        int mapMinWidth         = getResources().getInteger( R.integer.map_min_width );
        int mapMinHeight        = getResources().getInteger( R.integer.map_min_height );

        int mapTileMinWidth     = getResources().getInteger( R.integer.map_tile_min_width );
        int mapTileMinHeight    = getResources().getInteger( R.integer.map_tile_min_height );

        // Get user input
        String mapName          = mTbMapName.getText().toString();

        int mapTypeId           = mSpnMapType.getSelectedItemPosition();
        int mapWidth            = Utilities.parseInt(
                                        mTbMapWidth.getText().toString() , INVALID_SIZE );
        int mapHeight           = Utilities.parseInt(
                                        mTbMapHeight.getText().toString() , INVALID_SIZE );

        int mapTileTypeId       = mSpnMapTileType.getSelectedItemPosition();
        int mapTileWidth;
        int mapTileHeight;

        // If more setting is invisible, temporary show it to get the data than hide it back
        if( mIsMoreSettingVisible == false )
        {
            showMoreSettings( true );

            mapTileWidth = Utilities.parseInt(
                    mTbMapTileWidth.getText().toString(), INVALID_SIZE );
            mapTileHeight = Utilities.parseInt(
                    mTbMapTileHeight.getText().toString(), INVALID_SIZE );

            showMoreSettings( false );
        }
        else
        {
            mapTileWidth = Utilities.parseInt(
                    mTbMapTileWidth.getText().toString(), INVALID_SIZE );
            mapTileHeight = Utilities.parseInt(
                    mTbMapTileHeight.getText().toString(), INVALID_SIZE );
        }

        // Validate user input
        if( mapName.isEmpty() )
        {
            strErrMessage   += "\nInvalid map name";
            bIsValid        = false;
        }
        if( mapTypeId < 0 )
        {
            strErrMessage   += "\nInvalid map type";
            bIsValid        = false;
        }
        if( mapWidth < mapMinWidth )
        {
            strErrMessage   += "\nMap require a minimum width of " +
                                    mapMinWidth + " units";
            bIsValid        = false;
        }
        if( mapHeight < mapMinHeight )
        {
            strErrMessage   += "\nMap require a minimum height of " +
                                    mapMinHeight + " units";
            bIsValid        = false;
        }
        if( mapTileTypeId < 0 )
        {
            strErrMessage   += "\nInvalid map tile type";
            bIsValid        = false;
        }
        if( mapTileWidth < mapTileMinWidth )
        {
            strErrMessage   += "\nMap tile require a minimum width of " +
                                    mapTileMinWidth + " units";
            bIsValid        = false;
        }
        if( mapTileHeight < mapTileMinHeight )
        {
            strErrMessage   += "\nMap tile require a minimum height of " +
                                    mapTileMinHeight + " units";
            bIsValid        = false;
        }
        // Remove the first character (newline) of the err message string
        if( strErrMessage.isEmpty() == false )
        {
            strErrMessage = strErrMessage.substring( 1 );
        }

        if( bIsValid == false )
        {
            Log.d( TAG, "Map Properties validation was unsuccessful." );
            Toast.makeText( getActivity() , strErrMessage , Toast.LENGTH_SHORT ).show();
        }
        else
        {
            // Update the map with the validated value
            mMap.setMapName( mapName );
            mMap.setMapType( mapTypeId );
            mMap.setMapWidth( mapWidth );
            mMap.setMapHeight( mapHeight );
            mMap.setMapTileType( mapTileTypeId );
            mMap.setMapTileWidth( mapTileWidth );
            mMap.setMapTileHeight( mapTileHeight );

            Log.d( TAG, "Map Properties validation was successful." );
        }

        return bIsValid;
    }

    // This method would call the callback method to pass the new map properties data
    // to the map builder view to update the view.
    //
    protected void updateMapSettings()
    {
        if( validateMapProperties() == true )
        {
            Log.d( TAG, "Updating Map Settings..." );

            //Bundle bundle = createMapPropertiesBundle();
            if( mCallback != null )
            {
                //mCallback.onMapPropertiesUpdate( bundle );
            }
            else
            {
                Log.d( TAG, "Callback is not registered." );
            }

            Log.d( TAG, "Map Settings updated." );
        }
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Callback Interface and Methods

    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        if( Build.VERSION.SDK_INT < 23 )
        {
            mCallback = (IOnMapPropertiesUpdateListener)activity;
        }
    }

    @Override
    public void onAttach( Context context )
    {
        super.onAttach( context );

        if( context instanceof Activity )
        {
            mCallback = (IOnMapPropertiesUpdateListener)context;
        }
    }

    public interface IOnMapPropertiesUpdateListener
    {
        void onMapPropertiesUpdate( Map map );
    }

    // endregion
}
