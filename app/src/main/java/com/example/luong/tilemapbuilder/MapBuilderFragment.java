package com.example.luong.tilemapbuilder;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.luong.tilemapbuilder.Classes.Map;
import com.example.luong.tilemapbuilder.Classes.MapParcelable;
import com.example.luong.tilemapbuilder.Widgets.MapView;

/**
 * Map builder fragment used to display the tools to build a map.
 *
 * Created by Luong.
 */
public class MapBuilderFragment
        extends Fragment
{
    //////////////////////////////////////////////////
    // region // Constant Variable

    public final static String TAG = MapBuilderActivity.class.getSimpleName();

    // endregion

    //////////////////////////////////////////////////
    // region // Widget Controls

    MapView mMapView;

    // endregion

    //////////////////////////////////////////////////
    // region // Data Members

    protected Map mMap;

    // endregion

    //////////////////////////////////////////////////
    // region // Constructor

    public View onCreateView(
            LayoutInflater inflater , ViewGroup container ,
            Bundle savedInstanceState )
    {
        Log.d( TAG, "Creating Map Builder fragment..." );

        // Inflate the map builder layouts
        View view = inflater.inflate( R.layout.fragment_map_builder , null , false );

        initWidgets( view );
        regWidgetListeners();

        // Remove transparency from navigation drawer
        // mRoot.setScrimColor( Color.parseColor( "#00FFFFFF" ) );

        // Load data
        Bundle bundle = getArguments();
        if( bundle != null )
        {
            MapParcelable mapParcelable = bundle.getParcelable( "Map" );
            loadMap( mapParcelable.getMap() );
        }

        Log.d( TAG, "Map Builder fragment created." );

        return view;
    }

    protected void initWidgets( View view )
    {
        Log.d( TAG, "Initializing widget controls..." );

        mMapView    = (MapView)view.findViewById( R.id.map_view );

        Log.d( TAG, "Widget controls initialized." );
    }

    protected void regWidgetListeners()
    {
        Log.d( TAG, "Registering widget controls listener..." );

        Log.d( TAG, "Widget controls listener registered." );
    }

    // endregion



    //////////////////////////////////////////////////
    // region // Public Methods

    public boolean loadMap( Map map )
    {
        boolean isMapLoaded = false;

        if( map != null )
        {
            mMap = map;
        }

        return isMapLoaded;
    }

    // endregion
}
