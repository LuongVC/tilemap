Version Explaination
====================
<Massive Updates>.<Feature Implementation>.<Feature Building Blocks>.<Fix Update>

To Do List
==========
1.0.4.0     Draw the Orthographic Map Grid.

Change Log
==========

1.0.3.0		* Decouple Map code from UI code

1.0.2.3		* Divided the old Map Type into 2 separate category. Map Type for orthogonal view or 
			  isometric views, and Tile Tupe for 
			* Add "Show More Settings" text to hide uncommonly used settings.
			* Refactor Map Builder Activity layout structure.
			* Refactor Map Builder Fragment layout structure.
			* Refactor Map Builder Fragment class.

1.0.2.2		* Added Header to map properties in map builder.
			* Fix bug for Map Properties. Map Properties background is now solid white.
			* Refactor code.

1.0.2.1		* Fix bug for Map Creation Dialog. Dialog should stores the correct tile height value 
			  on default load.
			* Fix bug for Map Properties. Map Properties should stores the correct tile height 
			  value on default load.
			* Remove transparency from navigation drawer.
			* Refactor code.
			
1.0.2.0     * Slide from right to display map properties.
			* Fix crashing when accessing the preference settings for the first time.
			* Fix bug for Map Creation Dialog. Dialog should load default settings from user 
			  preference.
			* Fix bug for Map Creation Dialog. Dialog should passes the current data to Map Builder.

1.0.1.1		* Add more properties to preferences.
			* Have map creation dialog default values set to preference setting.
			
1.0.1.0     * Create new map dialog box (tile type, grid width & height).

1.0.0.0     * Preference settings (tile width & height settings).


			

			

			

			

			
