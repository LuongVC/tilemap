package com.example.luong.tilemapbuilder.Widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.example.luong.tilemapbuilder.Classes.Point2d;

/**
 * Created by Luong on 2016-08-05.
 */
public class MapView
        extends View
{
    //////////////////////////////////////////////////
    // Constant Variable

    public final static String TAG = MapView.class.getSimpleName();



    //////////////////////////////////////////////////
    // Data Members

    protected Paint mPaintGridColor;

    protected int mMapWidth;
    protected int mMapHeight;

    protected int mMapTileWidth;
    protected int mMapTileHeight;



    //////////////////////////////////////////////////
    // region // Constructors

    public MapView( Context context , AttributeSet attrs )
    {
        super( context );
    }

    // endregion



    //////////////////////////////////////////////////
    // region // Getter

    public Paint getGridColor(){ return mPaintGridColor; }

    public int getMapWidth(){ return mMapWidth; }
    public int getMapHeight(){ return mMapHeight; }

    public int getMapTileWidth(){ return mMapTileWidth; }
    public int getMapTileHeight(){ return mMapTileHeight; }

    // endregion



    //////////////////////////////////////////////////
    // region // Setter

    public void setGridColor( Paint gridColor )
    {
        mPaintGridColor = gridColor;
    }

    public void setMapWidth( int width )
    {
        mMapWidth = width;
        updateMapView();
    }

    public void setMapHeight( int height )
    {
        mMapHeight = height;
        updateMapView();
    }

    public void setMapTileWidth( int width )
    {
        mMapTileWidth = width;
        updateMapView();
    }

    public void setMapTileHeight( int height )
    {
        mMapTileHeight = height;
        updateMapView();
    }

    protected void updateMapView()
    {

    }

    // endregion
}
