package com.example.luong.tilemapbuilder.Classes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The map class keeps a list of tile layers making up the map design. It is design in a way so
 * that it is decouple from the UI class.
 *
 * Created by LuongVC.
 */

public class Map
{
    //////////////////////////////////////////////////
    // region // Constant Variables

    // endregion
    
    //////////////////////////////////////////////////
    // region // Member Variables

    protected String mMapName;
    
    protected int mMapTypeId;
    protected int mMapWidth;
    protected int mMapHeight;
    
    protected int mMapTileTypeId;
    protected int mMapTileWidth;
    protected int mMapTileHeight;
    
    // Map are made up of layers of tiles
    protected int mMapLayerMaxSize;
    protected int mMapLayerIndex;
    protected boolean mIsTileWithinBound;
    protected List<List<Tile>> mMapLayers;

    // endregion

    //////////////////////////////////////////////////
    // region // Constructors

    public Map()
    {
        initDefaultValues();
        instObject();

        mMapName            = generateUntitledMapName();
    }

    public Map( String name )
    {
        initDefaultValues();
        instObject();

        mMapName            = name;
    }

    public Map( String name , int width , int height )
    {
        initDefaultValues();
        instObject();

        mMapName            = name;
        mMapWidth           = width;
        mMapHeight          = height;
    }

    public Map( String name , int width , int height , boolean stayWithinBound )
    {
        initDefaultValues();
        instObject();

        mMapName            = name;
        mMapWidth           = width;
        mMapHeight          = height;
        mIsTileWithinBound  = stayWithinBound;
    }

    // Initialize default values
    //
    protected void initDefaultValues()
    {
        mMapTypeId          = 0;
        mMapWidth           = 1;
        mMapHeight          = 1;

        mMapTileTypeId      = 0;
        mMapTileWidth       = 64;
        mMapTileHeight      = 64;

        mMapLayerMaxSize    = 1;
        mIsTileWithinBound  = true;
    }

    // Instantiate object and list
    //
    protected void instObject()
    {
        mMapLayers = new ArrayList<List<Tile>>();
        for( int i = 0 ; i < mMapLayerMaxSize ; i++ )
        {
            mMapLayers.add( new ArrayList< Tile >() );
        }
    }

    // Returns a generated untitled map name
    //
    public String generateUntitledMapName()
    {
        DateFormat df       = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
        String date         = df.format( Calendar.getInstance().getTime() );
        String strMapName   = "Untitled_Map_" + date;

        return strMapName;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Copy Constructors

    public Map( Map map )
    {
        // Copy map data
        mMapTypeId = map.getMapTypeId();
        mMapWidth = map.getMapWidth();
        mMapHeight = map.getMapHeight();

        // Copy map tile data
        mMapTileTypeId = map.getMapTileTypeId();
        mMapTileWidth = map.getMapTileWidth();
        mMapTileHeight = map.getMapTileHeight();

        // Clear the map layers of tiles
        mMapLayers.clear();

        // Copy map layer
        for( List<Tile> curMapLayer : map.getMapLayers() )
        {
            // Create a new layer of tiles to copy from the source
            List<Tile> copyLayer = new ArrayList<>();

            // Copy all the tiles from the cur map layer to the copy map layer
            Iterator<Tile> tileIterator = curMapLayer.iterator();
            while( tileIterator.hasNext() )
            {
                copyLayer.add( new Tile( tileIterator.next() ) );
            }

            // Add the copy layer to the current class map layer
            mMapLayers.add( copyLayer );
        }
    }
    
    // endregion
    
    //////////////////////////////////////////////////
    // region // Setter Methods

    public void setMapName( String name )
    {
        mMapName = name;
    }
    
    public void setMapType( int id )
    { 
        mMapTypeId = id;
    }
    
    public void setMapWidth( int width )
    {
        mMapWidth = width;
    }
    
    public void setMapHeight( int height )
    {
        mMapHeight = height;
    }
    
    public void setMapTileType( int id )
    {
        mMapTileTypeId = id;
    }
    
    public void setMapTileWidth( int width )
    {
        mMapTileWidth = width;
    }
    
    public void setMapTileHeight( int height )
    {
        mMapTileHeight = height;
    }

    public void setMapLayerIndex( int index )
    {
        if( index < 0 )
        {
            mMapLayerIndex = 0;
        }
        else if( index > mMapLayerMaxSize )
        {
            mMapLayerIndex = mMapLayerMaxSize;
        }
        else
        {
            mMapLayerIndex = index;
        }
    }

    public void setMapLayerMaxSize( int size )
    {
        mMapLayerMaxSize = size;
    }
    
    // endregion
    
    //////////////////////////////////////////////////
    // region // Getter Methods

    public String getMapName(){ return mMapName; }
    
    public int getMapTypeId(){ return mMapTypeId; }
    
    public int getMapWidth(){ return mMapWidth; }
    
    public int getMapHeight(){ return mMapHeight; }
    
    public int getMapTileTypeId(){ return mMapTileTypeId; }
    
    public int getMapTileWidth(){ return mMapTileWidth; }
    
    public int getMapTileHeight(){ return mMapTileHeight; }

    public int getMapLayerIndex(){ return mMapLayerIndex; }

    public int getMapLayerSize(){ return mMapLayers.size(); }

    public int getMapLayerMaxSize(){ return mMapLayerMaxSize; }

    public List<List<Tile>> getMapLayers(){ return mMapLayers; }

    public List<Tile> getMapLayer( int index ){ return mMapLayers.get( index ); }
    
    // endregion
    
    //////////////////////////////////////////////////
    // region // Public Tile Methods

    // Add tile to the current active map layer
    //
    public boolean addTile( Tile tile )
    {
        return addTile( tile , mMapLayerIndex );
    }

    // Add tile to the specified map layer
    //
    public boolean addTile( Tile tile , int layerIndex )
    {
        boolean isTileAdded = false;

        // Check if layer index is valid
        if( layerIndex >= 0 && layerIndex < mMapLayers.size() )
        {
            // Get the current map layer
            List< Tile > mapLayer = mMapLayers.get( layerIndex );

            // Add tile to the map layer
            if( mIsTileWithinBound == true &&
                    tile.x >= 0 && tile.x < mMapWidth &&
                    tile.y >= 0 && tile.y < mMapHeight )
            {
                isTileAdded = mapLayer.add( tile );
            }else if( mIsTileWithinBound == false )
            {
                isTileAdded = mapLayer.add( tile );
            }
        }

        return isTileAdded;
    }

    // Remove tile from the current active map layer
    //
    public Tile removeTile( int x , int y )
    {
        return removeTile( x , y , mMapLayerIndex );
    }

    // Remove tile from the specified map layer
    //
    public Tile removeTile( int x , int y , int layerIndex )
    {
        Tile tileRemoved = null;

        // Check if layer index is valid
        if( layerIndex >= 0 && layerIndex < mMapLayers.size() )
        {
            // Get the current map layer
            List< Tile > mapLayer = mMapLayers.get( layerIndex );

            // Find the tile index and remove it
            Iterator< Tile > tileIterator = mapLayer.iterator();
            while( tileIterator.hasNext() && tileRemoved == null )
            {
                Tile curTile = tileIterator.next();
                if( curTile.x == x && curTile.y == y )
                {
                    tileRemoved = curTile;
                    tileIterator.remove();
                }
            }
        }
        
        return tileRemoved;
    }

    // Find tile from the current active map layer
    //
    public Tile findTile( int x , int y )
    {
        return findTile( x , y , mMapLayerIndex );
    }

    // Find tile from the specified map layer
    //
    public Tile findTile( int x , int y , int layerIndex )
    {
        Tile tileFound = null;

        // Check if layer index is valid
        if( layerIndex >= 0 && layerIndex < mMapLayers.size() )
        {
            // Get the current map layer
            List< Tile > mapLayer = mMapLayers.get( layerIndex );

            // Find the tile
            Iterator< Tile > tileIterator = mapLayer.iterator();
            while( tileIterator.hasNext() && tileFound == null )
            {
                Tile curTile = tileIterator.next();
                if( curTile.x == x && curTile.y == y )
                {
                    tileFound = curTile;
                }
            }
        }

        return tileFound;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Public Layer Methods

    public void addMapLayer( int index )
    {
        // Validate index value
        int boundIndex = 0;
        if( index < 0 )
        {
            boundIndex = 0;
        }
        else if( index >= mMapLayerMaxSize )
        {
            boundIndex = mMapLayerMaxSize - 1;
        }
        else
        {
            boundIndex = index;
        }

        mMapLayers.add( index , new ArrayList< Tile >() );
    }

    public void removeMapLayer( int index )
    {
        // Validate index value
        int boundIndex = 0;
        if( index < 0 )
        {
            boundIndex = 0;
        }
        else if( index >= mMapLayerMaxSize )
        {
            boundIndex = mMapLayerMaxSize - 1;
        }
        else
        {
            boundIndex = index;
        }

        mMapLayers.remove( index );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Public Map Methods

    // Return if the tiles added are to be within the bound of the map
    //
    public boolean isTileWithinBound()
    {
        return mIsTileWithinBound;
    }

    // Set and return if the tiles added are to be within the bound of the map
    //
    public boolean isTileWithinBound( boolean withinBound )
    {
        mIsTileWithinBound = withinBound;

        return mIsTileWithinBound;
    }

    // Remove all tiles that are out of bound from the map layer
    //
    public void updateMap()
    {
        // Loop through each map layers
        Iterator<List<Tile>> layerIterator = mMapLayers.iterator();
        while( layerIterator.hasNext() )
        {
            // Loop through each tiles
            Iterator<Tile> tileIterator = layerIterator.next().iterator();
            while( tileIterator.hasNext() )
            {
                // Check and remove tile if it is out of bound
                Tile curTile = tileIterator.next();
                if( curTile.x < 0 || curTile.x >= mMapWidth ||
                    curTile.y < 0 || curTile.y >= mMapHeight )
                {
                    tileIterator.remove();
                }
            }
        }
    }

    // endregion
}
