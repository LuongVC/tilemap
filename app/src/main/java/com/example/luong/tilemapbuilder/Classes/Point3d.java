package com.example.luong.tilemapbuilder.Classes;

/**
 * Basic class for holding 3d coordinates.
 *
 * Created by LuongVC.
 */

public class Point3d
    extends Point2d
{
    //////////////////////////////////////////////////
    // region //Data Members

    public float z;

    // endregion

    //////////////////////////////////////////////////
    // region // Constructors

    public Point3d()
    {
        x = 0.0f;
        y = 0.0f;
        z = 0.0f;
    }

    public Point3d( float x , float y , float z )
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Copy Constructors

    public Point3d( Point2d pt1 )
    {
        x = pt1.x;
        y = pt1.y;
        z = 0;
    }

    public Point3d( Point3d pt1 )
    {
        x = pt1.x;
        y = pt1.y;
        z = pt1.z;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Setters

    public void setZ( float z ){ this.z = z; }

    // endregion

    //////////////////////////////////////////////////
    // region // Getters

    public float getZ(){ return z; }

    // endregion
}
