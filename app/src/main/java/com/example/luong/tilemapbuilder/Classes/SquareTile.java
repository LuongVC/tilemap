package com.example.luong.tilemapbuilder.Classes;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 *
 * Created by LuongVC.
 */

public class SquareTile
{
    //////////////////////////////////////////////////
    // Data Members

    protected Paint mPaintOutline;
    protected float mFloatWidth;
    protected float mFloatHeight;



    //////////////////////////////////////////////////
    // region //Constructors

    public SquareTile( float width , float height )
    {
        mPaintOutline   = new Paint();
        mPaintOutline.setARGB( 255 , 255 , 255 , 255 );

        mFloatWidth     = width;
        mFloatHeight    = height;
    }

    public SquareTile( float width , float height , Paint outlineColour )
    {
        mFloatWidth     = width;
        mFloatHeight    = height;
        mPaintOutline   = outlineColour;
    }

    // endregion



    //////////////////////////////////////////////////
    // region // Draw Methods

    public void drawTile( float x , float y , Canvas canvas )
    {
        // Calculate coordinates of tile's corners
        Point2d pt2dTopLeft     = new Point2d( x , y );
        Point2d pt2dTopRight    = new Point2d( x + mFloatWidth , y );
        Point2d pt2dBottomLeft  = new Point2d( x , y + mFloatHeight );
        Point2d pt2BottomRight  = new Point2d( x + mFloatWidth , y + mFloatHeight );

        // Draw the tile lines
        canvas.drawLine(
                pt2dTopLeft.x , pt2dTopLeft.y ,
                pt2dTopRight.x , pt2dTopRight.y ,
                mPaintOutline );
        canvas.drawLine(
                pt2dBottomLeft.x , pt2dBottomLeft.y ,
                pt2BottomRight.x , pt2BottomRight.y ,
                mPaintOutline );
        canvas.drawLine(
                pt2dTopLeft.x , pt2dTopLeft.y ,
                pt2dBottomLeft.x , pt2dBottomLeft.y ,
                mPaintOutline );
        canvas.drawLine(
                pt2dTopRight.x , pt2dTopRight.y ,
                pt2BottomRight.x , pt2BottomRight.y ,
                mPaintOutline );
    }

    // endregion
}
