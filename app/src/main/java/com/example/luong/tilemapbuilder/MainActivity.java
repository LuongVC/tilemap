package com.example.luong.tilemapbuilder;

import android.app.DialogFragment;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.luong.tilemapbuilder.Classes.Map;
import com.example.luong.tilemapbuilder.Classes.MapParcelable;

/**
 * Main window application.
 *
 * Created by Luong.
 */

public class MainActivity
    extends AppCompatActivity
    implements CreateMapDialogFragment.IOnCreateMapDialogClickListener
{
    //////////////////////////////////////////////////
    // region // Constant Variables

    public final static String TAG = MainActivity.class.getSimpleName();

    // endregion

    //////////////////////////////////////////////////
    // region // Creation Methods

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        Log.d( TAG, "Creating Main Activity..." );

        setContentView( R.layout.activity_main );

        PreferenceManager.setDefaultValues( this , R.xml.main_settings, false );

        Log.d( TAG, "Main Activity created." );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Menu Methods

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        Log.d( TAG, "Creating options menu..." );

        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.main_menu, menu );

        Log.d( TAG, "Options menu created." );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch( item.getItemId() )
        {
            case R.id.menuAppSettingsAction:
                Log.d( TAG, "App Settings action was selected." );
                gotoMenuSettings();
                break;
            case R.id.menuCreateNewMapAction:
                Log.d( TAG, "Create New Map action was selected." );
                gotoMapCreation();
                break;
            default:
                Log.d( TAG, "Action not found." );
                return super.onOptionsItemSelected( item );
        }

        return true;
    }

    // This method will open the Menu Settings.
    //
    public void gotoMenuSettings()
    {
        Log.d( TAG, "Going to App Settings..." );

        Intent menuSettingsAction = new Intent( this , MainSettingsActivity.class );
        startActivity( menuSettingsAction );
    }

    // This method will generate a dialog pop-up with map properties require to create a new map.
    //
    public void gotoMapCreation()
    {
        Log.d( TAG, "Going to Map Creation..." );

        DialogFragment dialog = new CreateMapDialogFragment();
        dialog.show( getFragmentManager() , "CreateMapDialogFragment" );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Callback Methods

    @Override
    public void onDialogPositiveClick( DialogFragment dialog )
    {
        Map map = ((CreateMapDialogFragment)dialog).getMap();

        // Check if map is created
        if( map != null )
        {
            MapParcelable mapParcelable = new MapParcelable( map );

            // Create intent with bundle
            Intent intentMapBuilder = new Intent( this, MapBuilderActivity.class );
            intentMapBuilder.putExtra( "Map", mapParcelable );

            dialog.getDialog().dismiss();

            // Start Map Builder activity
            startActivity( intentMapBuilder );
        }
    }

    @Override
    public void onDialogNegativeClick( DialogFragment dialog )
    {
        dialog.getDialog().cancel();
    }

    // endregion
}
