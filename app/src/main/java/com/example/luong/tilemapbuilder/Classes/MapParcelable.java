package com.example.luong.tilemapbuilder.Classes;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * This parcel class wraps the Map class so that the Map data can be bundle up and pass along with
 * any intent activity without having to touch the original Map class.
 *
 * Created by Luong on 2016-09-11.
 */
public class MapParcelable
    implements Parcelable
{
    //////////////////////////////////////////////////
    // region // Constant variable

    public static final Creator< MapParcelable > CREATOR
            = new Creator< MapParcelable >()
    {
        @Override
        public MapParcelable createFromParcel( Parcel in )
        {
            return new MapParcelable( in );
        }

        @Override
        public MapParcelable[] newArray( int size )
        {
            return new MapParcelable[ size ];
        }
    };

    // endregion

    //////////////////////////////////////////////////
    // region // Member Variables

    private Map mMapData;

    // endregion

    //////////////////////////////////////////////////
    // region // Constructors

    public MapParcelable( Map map )
    {
        mMapData = map;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Setter Methods

    public void setMap( Map map ) { mMapData = map; }

    // endregion

    //////////////////////////////////////////////////
    // region // Getter Methods

    public Map getMap(){ return mMapData; }

    // endregion

    //////////////////////////////////////////////////
    // region // Read Methods

    private MapParcelable( Parcel in )
    {
        mMapData = new Map();

        // Read map data
        mMapData.setMapName( in.readString() );
        mMapData.setMapType( in.readInt() );
        mMapData.setMapWidth( in.readInt() );
        mMapData.setMapHeight( in.readInt() );

        // Read map tile data
        mMapData.setMapTileType( in.readInt() );
        mMapData.setMapTileWidth( in.readInt() );
        mMapData.setMapTileHeight( in.readInt() );

        // Read map layer data
        mMapData.setMapLayerMaxSize( in.readInt() );
        mMapData.setMapLayerIndex( in.readInt() );
        mMapData.isTileWithinBound( in.readByte() != 0 );

        // Read how many layers are in the map
        int numOfLayers = in.readInt();
        for( int layerIndex = 0 ; layerIndex < numOfLayers ; layerIndex++ )
        {
            // Read how many tiles are in the current layer
            int numOfTiles = in.readInt();

            // Read each tiles in a layer
            for( int x = 0 ; x < numOfTiles ; x ++ )
            {
                Tile tile = new Tile();
                tile.tileTypeId = in.readInt();
                tile.tileId     = in.readInt();
                tile.x          = in.readInt();
                tile.y          = in.readInt();

                mMapData.addTile( tile , layerIndex );
            }
        }
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Write Methods

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel out , int flags )
    {
        // Write map data
        out.writeString( mMapData.getMapName() );
        out.writeInt( mMapData.getMapTypeId() );
        out.writeInt( mMapData.getMapWidth() );
        out.writeInt( mMapData.getMapHeight() );

        // Write map tile data
        out.writeInt( mMapData.getMapTileTypeId() );
        out.writeInt( mMapData.getMapTileWidth() );
        out.writeInt( mMapData.getMapTileHeight() );

        // Write map layer data
        out.writeInt( mMapData.getMapLayerMaxSize() );
        out.writeInt( mMapData.getMapLayerIndex() );
        out.writeByte( (byte)( mMapData.isTileWithinBound() ? 1 : 0 ) );

        // Write how many layers are in the map
        out.writeInt( mMapData.getMapLayerSize() );
        for( int layerIndex = 0 ; layerIndex < mMapData.getMapLayerSize() ; layerIndex++ )
        {
            // Write how many tiles are in the current layer
            List<Tile> tiles = mMapData.getMapLayer( layerIndex );
            out.writeInt( tiles.size() );

            // Write the tiles in a layer
            for( Tile tile : tiles )
            {
                out.writeInt( tile.tileTypeId );
                out.writeInt( tile.tileId );
                out.writeInt( tile.x );
                out.writeInt( tile.y );
            }
        }
    }

    // endregion
}
