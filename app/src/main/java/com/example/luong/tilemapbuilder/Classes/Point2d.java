package com.example.luong.tilemapbuilder.Classes;

/**
 * Basic class for holding 2d coordinates.
 *
 * Created by LuongVC.
 */

public class Point2d
{
    //////////////////////////////////////////////////
    // region // Data Members

    public float x;
    public float y;

    // endregion

    //////////////////////////////////////////////////
    // region // Constructors

    public Point2d()
    {
        x = 0.0f;
        y = 0.0f;
    }

    public Point2d( float x , float y )
    {
        this.x = x;
        this.y = y;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Copy Constructors

    public Point2d( Point2d pt1 )
    {
        x = pt1.x;
        y = pt1.y;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Setters

    public void setX( float x ){ this.x = x; }
    public void setY( float y ){ this.y = y; }

    // endregion

    //////////////////////////////////////////////////
    // region // Getters

    public float getX(){ return x; }
    public float getY(){ return y; }

    // endregion
}
