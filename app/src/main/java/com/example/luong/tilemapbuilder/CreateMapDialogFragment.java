package com.example.luong.tilemapbuilder;


import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.luong.tilemapbuilder.Classes.Map;
import com.example.luong.tilemapbuilder.Classes.Utilities;

/**
 * This dialog fragment will generate a dialog with map properties require to generate a new map.
 *
 * Created by Luong.
 */
public class CreateMapDialogFragment
        extends DialogFragment
        implements AdapterView.OnItemSelectedListener
{
    //////////////////////////////////////////////////
    // region // Constant Variables

    public final static String  TAG = CreateMapDialogFragment.class.getSimpleName();

    public final static int     INVALID_SIZE        = -1;
    public final static int     INVALID_ID          = -1;

    // endregion

    //////////////////////////////////////////////////
    // region // Widget Controls

    protected EditText mTbMapName;

    protected Spinner mSpnMapType;
    protected Spinner mSpnMapTileType;

    protected EditText mTbMapWidth;
    protected EditText mTbMapHeight;

    // endregion

    //////////////////////////////////////////////////
    // region // Data Members

    protected String mMapName;

    protected int mMapTypeId;
    protected int mMapWidth;
    protected int mMapHeight;

    protected int mMapTileTypeId;
    protected int mMapTileWidth;
    protected int mMapTileHeight;

    // endregion

    //////////////////////////////////////////////////
    // region // Callback Members

    protected IOnCreateMapDialogClickListener mCallback;

    // endregion

    //////////////////////////////////////////////////
    // region // Dialog Creation Methods

    @Override
    public Dialog onCreateDialog( Bundle saveInstanceState )
    {
        Log.d( TAG, "Creating dialog..." );

        // Create dialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

        // inflate the dialog layout
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate( R.layout.dialog_map_properties, null );

        // Setup the widget controls
        initWidgets( view );
        populateMapTypeSpinner();
        populateMapTileTypeSpinner();
        regWidgetListeners();
        loadDefaultValues();

        // Set the view
        builder.setView( view );

        // Set title
        builder.setTitle( getResources().getString( R.string.txt_title_map_properties ) );

        // Set button actions
        builder.setPositiveButton( R.string.create_map_action, null );
        builder.setNegativeButton( R.string.btn_cancel, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick( DialogInterface dialogInterface, int i )
                {
                    Log.d( TAG, "Cancel action selected." );

                    if( mCallback != null )
                    {
                        mCallback.onDialogNegativeClick( CreateMapDialogFragment.this );
                    }
                    else
                    {
                        Log.d( TAG, "Callback not registered." );
                    }
                }
            } );

        // Create the dialog
        final AlertDialog dialog = builder.create();

        // Override the positive button action to prevent closing of the AlertDialog
        dialog.setOnShowListener( new DialogInterface.OnShowListener(){
            @Override
            public void onShow( DialogInterface dialogInterface )
            {
                Button btnPositive = dialog.getButton( AlertDialog.BUTTON_POSITIVE );
                btnPositive.setOnClickListener( new View.OnClickListener(){
                    @Override
                    public void onClick( View view )
                    {
                        String strButton = getResources().getString( R.string.create_map_action );
                        Log.d( TAG, strButton + " action selected." );

                        if( mCallback != null )
                        {
                            mCallback.onDialogPositiveClick( CreateMapDialogFragment.this );
                        }
                        else
                        {
                            Log.d( TAG, "Callback not registered." );
                        }
                    }
                } );
            }
        } );

        Log.d( TAG, "Dialog created." );

        return dialog;
    }

    // Method used to initialize variables for widget controls.
    //
    protected void initWidgets( View view )
    {
        Log.d( TAG, "Initialize widget controls..." );

        // Get widget controls
        mTbMapName          = (EditText)view.findViewById( R.id.tb_map_name );
        mSpnMapType         = (Spinner)view.findViewById( R.id.spn_map_type );
        mSpnMapTileType     = (Spinner)view.findViewById( R.id.spn_map_tile_type );

        mTbMapWidth         = (EditText)view.findViewById( R.id.tb_map_width );
        mTbMapHeight        = (EditText)view.findViewById( R.id.tb_map_height );

        Log.d( TAG, "Widget controls initialized" );
    }

    // Method used to assigned, register, and setup listeners for widget controls.
    //
    protected void regWidgetListeners()
    {
        Log.d( TAG, "Registering widget controls listener..." );

        // Set widget listener
        mSpnMapType.setOnItemSelectedListener( this );
        mSpnMapTileType.setOnItemSelectedListener( this );

        Log.d( TAG, "Widget controls listener registered." );
    }

    // Creates an adapter to populate the MapType dropdown list.
    //
    protected void populateMapTypeSpinner()
    {
        Log.d( TAG, "Populating Map Type dropdown list..." );

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource( getActivity() ,
                R.array.map_type_array , android.R.layout.simple_spinner_item );
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        mSpnMapType.setAdapter( adapter );

        Log.d( TAG, "Map Type dropdown list populated." );
    }

    // Creates an adapter to populate the MapTileType dropdown list.
    //
    protected void populateMapTileTypeSpinner()
    {
        Log.d( TAG, "Populating Map Tile Type dropdown list..." );

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource( getActivity() ,
                R.array.map_tile_type_array , android.R.layout.simple_spinner_item );
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        mSpnMapTileType.setAdapter( adapter );

        Log.d( TAG, "Map Tile Type dropdown list populated." );
    }

    // Method use to load the default values preference and xml values for the class.
    //
    protected void loadDefaultValues()
    {
        Log.d( TAG, "Loading default values..." );
        
        // Get default values for preferences
        String mapType      = getResources().getString( R.string.pref_summary_map_type );
        int mapWidth        = getResources().getInteger( R.integer.pref_value_map_size_width );
        int mapHeight       = getResources().getInteger( R.integer.pref_value_map_size_height );

        String mapTileType  = getResources().getString( R.string.pref_summary_tile_type );
        int mapTileWidth    = getResources().getInteger( R.integer.pref_value_tile_size_width );
        int mapTileHeight   = getResources().getInteger( R.integer.pref_value_tile_size_height );

        // Get default values from user preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( getActivity() );

        mapType             = prefs.getString( "prefMapType" , mapType );
        ArrayAdapter aaMapType = (ArrayAdapter)mSpnMapType.getAdapter();
        //mMapTypeId          = aaMapType.getPosition( mapType );
        mMapTypeId          = Utilities.parseInt( mapType , INVALID_ID );

        mMapWidth           = Integer.parseInt( prefs.getString( "prefMapWidth" ,
                                    Integer.toString( mapWidth ) ) );
        mMapHeight          = Integer.parseInt( prefs.getString( "prefMapHeight" ,
                                    Integer.toString( mapHeight ) ) );

        mapTileType         = prefs.getString( "prefMapTileType" , mapType );
        ArrayAdapter aaMapTileType = (ArrayAdapter)mSpnMapTileType.getAdapter();
        //mMapTileTypeId      = aaMapTileType.getPosition( mapTileType );
        mMapTileTypeId      = Utilities.parseInt( mapTileType , INVALID_ID );

        mMapTileWidth       = Integer.parseInt( prefs.getString( "prefMapTileWidth" ,
                                    Integer.toString( mapTileWidth ) ) );
        mMapTileHeight      = Integer.parseInt( prefs.getString( "prefMapTileHeight" ,
                                    Integer.toString( mapTileHeight ) ) );

        String message = "Retrieving preference values..." +
                "\nMap Type: " + mMapTypeId + "(" + mapType + ")" +
                "\t\tMap Tile Type: " + mMapTileTypeId + "(" + mapTileType + ")" +
                "\nMap Width: " + mMapWidth +
                "\t\tMap Height: " + mMapHeight;
        Log.d( TAG, message );

        // Set default values
        mSpnMapType.setSelection( mMapTypeId );
        mTbMapWidth.setText( Integer.toString( mMapWidth ) );
        mTbMapHeight.setText( Integer.toString( mMapHeight ) );

        mSpnMapTileType.setSelection( mMapTypeId );

        Log.d( TAG, "Default values loaded." );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Getter Methods

    // Returns a validated map containing the all the map properties entered by the user.
    //
    public Map getMap()
    {
        Map map = null;
        if( validateMapProperties() == true )
        {
            map = createMap();
        }

        return map;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Override Listener Methods

    @Override
    public void onItemSelected( AdapterView< ? > adapterView, View view, int i, long l )
    {
        switch( adapterView.getId() )
        {
            case R.id.spn_map_type:
                mMapTypeId = adapterView.getSelectedItemPosition();
                Log.d( TAG, "Changing Map Type: " + mMapTypeId );
                break;
            case R.id.spn_map_tile_type:
                mMapTileTypeId = adapterView.getSelectedItemPosition();
                Log.d( TAG, "Changing Map Tile Type: " + mMapTileTypeId );
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected( AdapterView< ? > adapterView )
    {

    }

    // endregion

    //////////////////////////////////////////////////
    // region // Confirmation Methods

    // Returns whether or not all the user input are valid.
    //
    protected boolean validateMapProperties()
    {
        Log.d( TAG, "Validating Map Properties..." );
        
        boolean bIsValid        = true;
        String strErrMessage    = "";

        // Get resource values
        int intMapMinWidth      = getResources().getInteger( R.integer.map_min_width );
        int intMapMinHeight     = getResources().getInteger( R.integer.map_min_height );

        // Get user input
        mMapName                = mTbMapName.getText().toString();
        mMapTypeId              = mSpnMapType.getSelectedItemPosition();
        mMapTileTypeId          = mSpnMapTileType.getSelectedItemPosition();

        mMapWidth               = Utilities.parseInt(
                                        mTbMapWidth.getText().toString() , INVALID_SIZE );
        mMapHeight              = Utilities.parseInt(
                                        mTbMapHeight.getText().toString() , INVALID_SIZE );

        // Validate user input
        if( mMapName.isEmpty() )
        {
            strErrMessage   += "\nInvalid map name";
            bIsValid        = false;
        }
        if( mMapTypeId < 0 )
        {
            strErrMessage   += "\nRequire a map type";
            bIsValid        = false;
        }
        if( mMapTileTypeId < 0 )
        {
            strErrMessage   += "\nRequire a map tile type";
            bIsValid        = false;
        }
        if( mMapWidth < intMapMinWidth )
        {
            strErrMessage   += "\nRequire a minimum map width of " + intMapMinWidth + " units";
            bIsValid        = false;
        }
        if( mMapHeight < intMapMinHeight )
        {
            strErrMessage   += "\nRequire a minimum map height of " + intMapMinHeight + " units";
            bIsValid        = false;
        }
        // Remove the first character (newline) of the err message string
        if( strErrMessage.isEmpty() == false )
        {
            strErrMessage = strErrMessage.substring( 1 );
        }

        if( bIsValid == false )
        {
            Log.d( TAG, "Map Properties validation was unsuccessful.\n" + strErrMessage );
            Toast.makeText( getActivity() , strErrMessage , Toast.LENGTH_SHORT ).show();
        }
        else
        {
            Log.d( TAG, "Map Properties validation was successful" );
        }

        return bIsValid;
    }

    // Returns a map containing all the map properties data regardless whether it is valid or not
    //
    protected Map createMap()
    {
        // Get default values from user preferences
        int intMapTileWidth     = getResources().getInteger( R.integer.pref_value_tile_size_width );
        int intMapTileHeight    = getResources().getInteger( R.integer.pref_value_tile_size_height );

        // Get default tile size from user preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( getActivity() );
        mMapTileWidth           = Integer.parseInt( prefs.getString( "prefMapTileWidth" ,
                                        Integer.toString( intMapTileWidth ) ) );
        mMapTileHeight          = Integer.parseInt( prefs.getString( "prefMapTileHeight" ,
                                        Integer.toString( intMapTileHeight ) ) );

        // Create a map
        Map map = new Map();
        map.setMapName( mMapName );
        map.setMapType( mMapTypeId );
        map.setMapWidth( mMapWidth );
        map.setMapHeight( mMapHeight );
        map.setMapTileType( mMapTileTypeId );
        map.setMapTileWidth( mMapTileWidth );
        map.setMapTileHeight( mMapTileHeight );

        String message = "Creating Map..." +
                "\nName: " + mMapName +
                "\nMap Type: " + mMapTypeId +
                "\t\t\tMap Width: " + mMapWidth +
                "\t\t\tMap Height: " + mMapHeight +
                "\nMap Tile Type: " + mMapTileTypeId +
                "\t\tMap Tile Width: " + mMapTileWidth +
                "\t\tMap Tile Height: " + mMapTileHeight;
        Log.d( TAG, message );

        return map;
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Callback methods

    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        if( Build.VERSION.SDK_INT < 23 )
        {
            mCallback = (IOnCreateMapDialogClickListener)activity;
        }
    }

    @Override
    public void onAttach( Context context )
    {
        super.onAttach( context );

        if( context instanceof Activity )
        {
            mCallback = (IOnCreateMapDialogClickListener)context;
        }
    }

    public interface IOnCreateMapDialogClickListener
    {
        public void onDialogPositiveClick( DialogFragment dialog );
        public void onDialogNegativeClick( DialogFragment dialog );
    }

    // endregion
}
