package com.example.luong.tilemapbuilder;

import android.os.Bundle;
import android.util.Log;

/**
 * Settings activity for the app main window.
 *
 * Created by Luong.
 */
public class MainSettingsActivity
        extends SubActivity
{
    //////////////////////////////////////////////////
    // region // Constant Variables

    public final static String TAG = MainSettingsActivity.class.getSimpleName();

    // endregion

    //////////////////////////////////////////////////
    // region // Creation Methods

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        Log.d( TAG, "Creating Main Settings activity..." );
        
        MainSettingsFragment fragment = new MainSettingsFragment();
        getFragmentManager().beginTransaction()
            .replace( android.R.id.content , fragment , fragment.getClass().getSimpleName() )
            .commit();

        Log.d( TAG, "Main Settings activity created." );
    }

    // endregion
}
