package com.example.luong.tilemapbuilder.Classes;

/**
 * Created by Luong on 2016-09-11.
 */
public class TileFactory
{
    //////////////////////////////////////////////////
    // region // Constant Variables

    public final static int BASTE_TILE      = 0;
    public final static int SQUARE_TILE     = 1;
    public final static int HEXAGON_TILE    = 2;

    // endregion
}
