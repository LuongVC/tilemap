package com.example.luong.tilemapbuilder;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Settings fragment for the app main window.
 *
 * Created by Luong.
 */
public class MainSettingsFragment
    extends PreferenceFragment
    implements SharedPreferences.OnSharedPreferenceChangeListener
{
    //////////////////////////////////////////////////
    // region // Constant Variables

    public final static String TAG = MainSettingsFragment.class.getSimpleName();

    // endregion

    //////////////////////////////////////////////////
    // region // Data Members

    private SharedPreferences prefs;
    public final static String PREF_STRING_ERROR    = "#ERROR!";

    public final static String PREF_MAP_TYPE        = "prefMapType";
    public final static String PREF_MAP_WIDTH       = "prefMapWidth";
    public final static String PREF_MAP_HEIGHT      = "prefMapHeight";

    public final static String PREF_TILE_TYPE       = "prefMapTileType";
    public final static String PREF_TILE_WIDTH      = "prefMapTileWidth";
    public final static String PREF_TILE_HEIGHT     = "prefMapTileHeight";

    public final static int FIRST_INDEX_POSITION    = 0;

    // endregion

    //////////////////////////////////////////////////
    // region // Creation Methods

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        Log.d( TAG, "Creating Main Settings fragment..." );
        
        addPreferencesFromResource( R.xml.main_settings );
        loadPreferenceSettings();

        Log.d( TAG, "Main Settings fragment created." );
    }

    private void loadPreferenceSettings()
    {
        Log.d( TAG, "Loading Preference Settings..." );

        prefs = PreferenceManager.getDefaultSharedPreferences( getActivity() );
        prefs.registerOnSharedPreferenceChangeListener( this );



        // Map Settings
        ListPreference prefMapType = (ListPreference)findPreference( PREF_MAP_TYPE );
        if( prefMapType != null && prefMapType.getEntry() != null )
        {
            String summaryValue = prefMapType.getEntry().toString();
            prefMapType.setSummary( summaryValue );
        }
        else
        {
            int defaultValue = FIRST_INDEX_POSITION;
            prefMapType.setValueIndex( defaultValue );
            prefMapType.setSummary( prefMapType.getEntry().toString() );
        }

        Preference prefMapWidth = findPreference( PREF_MAP_WIDTH );
        if( prefMapWidth != null )
        {
            String summaryValue = prefs.getString( PREF_MAP_WIDTH, PREF_STRING_ERROR );
            prefMapWidth.setSummary( summaryValue );
        }
        else
        {
            int defaultValue = getResources().getInteger( R.integer.pref_value_map_size_width );
            prefMapWidth.setSummary( defaultValue );
        }

        Preference prefMapHeight = findPreference( PREF_MAP_HEIGHT );
        if( prefMapHeight != null )
        {
            String summaryValue = prefs.getString( PREF_MAP_HEIGHT, PREF_STRING_ERROR );
            prefMapHeight.setSummary( summaryValue );
        }
        else
        {
            int defaultValue = getResources().getInteger( R.integer.pref_value_map_size_height );
            prefMapHeight.setSummary( defaultValue );
        }



        // Tile Settings
        ListPreference prefTileType = (ListPreference)findPreference( PREF_TILE_TYPE );
        if( prefTileType != null && prefTileType.getEntry() != null )
        {
            String summaryValue = prefTileType.getEntry().toString();
            prefTileType.setSummary( summaryValue );
        }
        else
        {
            int defaultValue = FIRST_INDEX_POSITION;
            prefTileType.setValueIndex( defaultValue );
            prefTileType.setSummary( prefTileType.getEntry().toString() );
        }

        Preference prefMapTileWidth = findPreference( PREF_TILE_WIDTH );
        if( prefMapTileWidth != null )
        {
            String summaryValue = prefs.getString( PREF_TILE_WIDTH, PREF_STRING_ERROR );
            prefMapTileWidth.setSummary( summaryValue );
        }
        else
        {
            int defaultValue = getResources().getInteger( R.integer.pref_value_tile_size_width );
            prefMapTileWidth.setSummary( defaultValue );
        }

        Preference prefMapTileHeight = findPreference( PREF_TILE_HEIGHT );
        if( prefMapTileHeight != null )
        {
            String summaryValue = prefs.getString( PREF_TILE_HEIGHT, PREF_STRING_ERROR );
            prefMapTileHeight.setSummary( summaryValue );
        }
        else
        {
            int defaultValue = getResources().getInteger( R.integer.pref_value_tile_size_height );
            prefMapTileHeight.setSummary( defaultValue );
        }

        Log.d( TAG, "Preference Settings loaded." );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Lifecycle Methods

    @Override
    public void onResume()
    {
        super.onResume();

        getPreferenceScreen().getSharedPreferences()
            .registerOnSharedPreferenceChangeListener( this );
    }

    @Override
    public void onPause()
    {
        super.onPause();

        getPreferenceScreen().getSharedPreferences()
            .unregisterOnSharedPreferenceChangeListener( this );
    }

    // endregion

    //////////////////////////////////////////////////
    // region // Preference Methods

    @Override
    public void onSharedPreferenceChanged( SharedPreferences sharedPreferences, String key )
    {
        Preference preference = findPreference( key );

        if( key.equals( PREF_MAP_TYPE ) )
        {
            preference.setSummary( ( (ListPreference)preference ).getEntry() );

            Log.d( TAG, "Updating Map Type: " + ( (ListPreference)preference ).getEntry() );
        }
        else if( key.equals( PREF_MAP_WIDTH ) )
        {
            preference.setSummary( sharedPreferences.getString( key , PREF_STRING_ERROR ) );

            Log.d( TAG, "Updating Map Width: " + preference.getSummary() );
        }
        else if( key.equals( PREF_MAP_HEIGHT ) )
        {
            preference.setSummary( sharedPreferences.getString( key , PREF_STRING_ERROR ) );

            Log.d( TAG, "Updating Map Height: " + preference.getSummary() );
        }
        else if( key.equals( PREF_TILE_TYPE ) )
        {
            preference.setSummary( ( (ListPreference)preference ).getEntry() );

            Log.d( TAG, "Updating Tile Type: " + preference.getSummary() );
        }
        else if( key.equals( PREF_TILE_WIDTH ) )
        {
            preference.setSummary( sharedPreferences.getString( key , PREF_STRING_ERROR ) );

            Log.d( TAG, "Updating Tile Width: " + preference.getSummary() );
        }
        else if( key.equals( PREF_TILE_HEIGHT ) )
        {
            preference.setSummary( sharedPreferences.getString( key , PREF_STRING_ERROR ) );

            Log.d( TAG, "Updating Tile Height: " + preference.getSummary() );
        }
        else
        {
            Log.d( TAG, "Unknown Preference Settings: " + key + " = " + preference.getSummary() );
        }
    }

    // endregion
}
