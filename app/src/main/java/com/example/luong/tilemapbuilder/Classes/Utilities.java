package com.example.luong.tilemapbuilder.Classes;

import android.util.Log;

/**
 * Utility class holding methods for frequently used block of code to perform mundane task.
 * Created by Luong.
 */

public class Utilities
{
    //////////////////////////////////////////////////
    // Constant Variables

    public final static String  TAG = Utilities.class.getSimpleName();

    //////////////////////////////////////////////////
    // Utility Methods

    public static int parseInt( String value , int defaultValue )
    {
        int intValue = 0;

        try
        {
            intValue = Integer.parseInt( value );
        }
        catch( NumberFormatException e )
        {
            intValue = defaultValue;
        }

        return intValue;
    }
}
